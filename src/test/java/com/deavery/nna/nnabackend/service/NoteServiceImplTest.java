package com.deavery.nna.nnabackend.service;

import com.deavery.nna.nnabackend.NnaBackendApplication;
import com.deavery.nna.nnabackend.model.Note;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigInteger;


@SpringBootTest(classes = NnaBackendApplication.class)
@AutoConfigureMockMvc
public class NoteServiceImplTest {
    @Autowired
    private NoteService noteService;

    @Test
    public void testValidateCreate() {
        Assertions.assertTrue(noteService.isWorks());
        Assertions.assertTrue(noteService.validateCreate(new Note(null, BigInteger.TWO, "Test", null)));
        Assertions.assertFalse(noteService.validateCreate(new Note(null, BigInteger.TWO, null, null)));
        Assertions.assertFalse(noteService.validateCreate(new Note(BigInteger.ZERO, null, "Test", null)));
    }

    @Test
    public void testValidateUpdate() {
        Assertions.assertTrue(noteService.isWorks());
        Assertions.assertTrue(noteService.validateUpdate(new Note(BigInteger.TWO, null, "Test", null)));
        Assertions.assertFalse(noteService.validateUpdate(new Note(null, BigInteger.TWO, null, null)));
        Assertions.assertFalse(noteService.validateUpdate(new Note(BigInteger.ZERO, null, null, "Test")));
    }

    @Test
    public void testValidateGetNotesFromUserId() {
        Assertions.assertTrue(noteService.isWorks());
        Assertions.assertTrue(noteService.validateGetNotesFromUserId(BigInteger.TWO));
        Assertions.assertFalse(noteService.validateGetNotesFromUserId(BigInteger.ZERO));
    }

    @Test
    public void testValidateDeleteNoteFromId() {
        Assertions.assertTrue(noteService.isWorks());
        Assertions.assertTrue(noteService.validateDeleteNoteFromId(BigInteger.TWO));
        Assertions.assertFalse(noteService.validateDeleteNoteFromId(BigInteger.ZERO));
    }
}
