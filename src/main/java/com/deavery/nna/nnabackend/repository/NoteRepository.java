package com.deavery.nna.nnabackend.repository;

import com.deavery.nna.nnabackend.model.Note;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface NoteRepository {
    Object createNote(Note note);

    List<Object> getNotesFromUserId(BigInteger userId);

    Object updateNote(Note note);

    Object deleteNoteFromId(BigInteger id);
}
