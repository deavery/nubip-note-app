package com.deavery.nna.nnabackend.repository;

import com.deavery.nna.nnabackend.model.Note;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@AllArgsConstructor
@Repository
public class NoteRepositoryImpl implements NoteRepository {

    private final JdbcTemplate jdbcTemplate;

    private static Note mapNote(ResultSet rs, int rowNum) throws SQLException {
        return new Note(
                BigInteger.valueOf(rs.getInt("id")),
                BigInteger.valueOf(rs.getInt("user_id")),
                rs.getString("title"),
                rs.getString("note")
        );
    }
    @Override
    public Object createNote(Note note) {
        return jdbcTemplate.query(
                "INSERT INTO public.note(user_id, title, note) VALUES(?, ?, ?) RETURNING id, user_id, title, note",
                NoteRepositoryImpl::mapNote,
                note.getUserId(),
                note.getTitle(),
                note.getNote()
        );
    }

    @Override
    public List<Object> getNotesFromUserId(BigInteger userId) {
        return jdbcTemplate.query(
                "SELECT id, user_id, title, note from public.note WHERE user_id = ?",
                NoteRepositoryImpl::mapNote,
                userId
        );
    }

    @Override
    public Object updateNote(Note note) {
        return jdbcTemplate.query(
                "UPDATE public.note SET title = ?, note = ? WHERE id = ? RETURNING id, user_id, title, note",
                NoteRepositoryImpl::mapNote,
                note.getTitle(),
                note.getNote(),
                note.getId()
        );
    }

    @Override
    public Object deleteNoteFromId(BigInteger id) {
        return jdbcTemplate.query(
                "DELETE FROM public.note WHERE id = ? RETURNING id, user_id, title, note",
                NoteRepositoryImpl::mapNote,
                id
        );
    }
}
