package com.deavery.nna.nnabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NnaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(NnaBackendApplication.class, args);
    }

}
