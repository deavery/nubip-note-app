package com.deavery.nna.nnabackend.service;

import com.deavery.nna.nnabackend.model.Note;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public interface NoteService {
    List<Object> getNotesFromUserId(BigInteger userId);

    Object createNote(Note note);

    Object updateNote(Note note);

    Object deleteNoteFromId(BigInteger id);

    boolean validateUpdate(Note note);

    boolean validateGetNotesFromUserId(BigInteger id);

    boolean validateCreate(Note note);

    boolean validateDeleteNoteFromId(BigInteger id);

    boolean isWorks();
}
