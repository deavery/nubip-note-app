package com.deavery.nna.nnabackend.service;

import com.deavery.nna.nnabackend.model.Note;
import com.deavery.nna.nnabackend.repository.NoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@Service
@Transactional
public class NoteServiceImpl implements NoteService {
    private final NoteRepository noteRepository;

    @Override
    public List<Object> getNotesFromUserId(BigInteger userId) {
        if (!validateGetNotesFromUserId(userId)) throw new IllegalArgumentException();
        return noteRepository.getNotesFromUserId(userId);
    }

    @Override
    public Object createNote(Note note) {
        if (!validateCreate(note)) throw new IllegalArgumentException();
        return noteRepository.createNote(note);
    }

    @Override
    public Object updateNote(Note note) {
        if (!validateUpdate(note)) throw new IllegalArgumentException();
        return noteRepository.updateNote(note);
    }

    @Override
    public Object deleteNoteFromId(BigInteger id) {
        if (!validateDeleteNoteFromId(id)) throw new IllegalArgumentException();
        return noteRepository.deleteNoteFromId(id);
    }

    @Override
    public boolean validateCreate(Note note) {
        return Objects.nonNull(note.getUserId()) && Objects.nonNull(note.getTitle());
    }

    @Override
    public boolean validateUpdate(Note note) {
        return Objects.nonNull(note.getId()) && Objects.nonNull(note.getTitle());
    }

    @Override
    public boolean validateGetNotesFromUserId(BigInteger userId) {
        return userId.shortValueExact() != BigInteger.ZERO.shortValueExact();
    }

    @Override
    public boolean validateDeleteNoteFromId(BigInteger id) {
        return id.shortValueExact() != BigInteger.ZERO.shortValueExact();
    }

    @Override
    public boolean isWorks() {
        return true;
    }
}
