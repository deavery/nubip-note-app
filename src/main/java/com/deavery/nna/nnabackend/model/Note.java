package com.deavery.nna.nnabackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Note {
    BigInteger id;
    BigInteger userId;
    String title;
    String note;
}
