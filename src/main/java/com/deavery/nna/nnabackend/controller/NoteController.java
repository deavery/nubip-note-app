package com.deavery.nna.nnabackend.controller;

import com.deavery.nna.nnabackend.model.Note;
import com.deavery.nna.nnabackend.service.NoteService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequestMapping("/api/note")
@AllArgsConstructor
@Slf4j
public class NoteController {

    private final NoteService noteService;

    @PostMapping
    public ResponseEntity<?> createNote(
            @RequestBody Note note
    ) {
        return ResponseEntity.ok(noteService.createNote(note));
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateNote(
            @RequestBody Note note
    ) {
        return ResponseEntity.ok(noteService.updateNote(note));
    }

    @GetMapping
    public ResponseEntity<?> getNotes(
            @RequestParam BigInteger userId
    ) {
        return ResponseEntity.ok(noteService.getNotesFromUserId(userId));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteNote(
            @RequestParam BigInteger id
    ) {
        return ResponseEntity.ok(noteService.deleteNoteFromId(id));
    }
}
